# Packages
Ansible role to install / uninstall packages.

## Requirements

* Ansible >= 4

## OS

* Debian

## Role Variables

Variable name  | Variable description
---            | ---
packages       | list of packages to install / unintall
packages_state | "present" or "absent" to install or uninstall packages

## Dependencies

No.

## Playbook Example

Install packages :

```
- name: Install packages
  hosts: all
  become: yes
  roles:
    - role: packages
      packages_state: present
```

Uninstall packages :

```
- name: Uninstall packages
  hosts: all
  become: yes
  roles:
    - role: packages
      packages_state: absent
```


## License

GPL v3

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)
